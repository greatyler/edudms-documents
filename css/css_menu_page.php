<?php


function news_css_register_submenu_page() {
	add_theme_page( 'EduDMS News Appearance', 'News Appearance', 'edit_theme_options', 'news_css_menu', 'news_css_render_submenu_page' );
}
add_action( 'admin_menu', 'news_css_register_submenu_page' );




function news_css_register_settings() {
	register_setting( 'news_css_settings_group', 'news_css_settings' );
}
add_action( 'admin_init', 'news_css_register_settings' );


function news_css_render_submenu_page() {

	$options = get_option( 'news_css_settings' );
	$content = isset( $options['news_css-content'] ) && ! empty( $options['news_css-content'] ) ? $options['news_css-content'] : __( '/* Enter Your Custom CSS Here */', 'news-css' );

	if ( isset( $_GET['settings-updated'] ) ) : ?>
		<div id="message" class="updated"><p>Custom CSS updated successfully.</p></div>
	<?php endif; ?>
	<div class="wrap">
		<h2 style="margin-bottom: 1em;">CSS Settings</h2>
		<form name="news_css-form" action="options.php" method="post" enctype="multipart/form-data">
			<?php settings_fields( 'news_css_settings_group' ); ?>
			<div id="templateside">
				<?php do_action( 'news_css-sidebar-top' ); ?>
				<p style="margin-top: 0">Adjust the css of the news addon</p>
				<p>To use, read comments carefully, alter hex values (#???), then click "Update News CSS".</p>
				<?php submit_button( 'Update News CSS', 'primary', 'submit', true ); ?>
				<?php do_action( 'news_css-sidebar-bottom' ); ?>
			</div>
			<div id="template">
				<?php do_action( 'news_css-form-top' ); ?>
				<div>
					<textarea cols="70" rows="30" name="news_css_settings[news_css-content]" id="news_css_settings[news_css-content]" ><?php echo esc_html( $content ); ?></textarea>
				</div>
				<?php do_action( 'news_css-textarea-bottom' ); ?>
				<div>
					<?php submit_button( 'Update News CSS', 'primary', 'submit', true ); ?>
				</div>
				<?php do_action( 'news_css-form-bottom' ); ?>
			</div>
		</form>
		
	</div>

<?php
}



function news_css_register_style() {
	$url = home_url();

	if ( is_ssl() ) {
		$url = home_url( '/', 'https' );
	}

	wp_register_style( 'news_css_style', add_query_arg( array( 'news_css' => 1 ), $url ) );

	wp_enqueue_style( 'news_css_style' );
}
add_action( 'wp_enqueue_scripts', 'news_css_register_style', 99 );

/**
 * If the query var is set, print the Simple Custom CSS rules.
 */
function news_css_maybe_print_css() {

	// Only print CSS if this is a stylesheet request
	if( ! isset( $_GET['news_css'] ) || intval( $_GET['news_css'] ) !== 1 ) {
		return;
	}

	ob_start();
	header( 'Content-type: text/css' );
	$options     = get_option( 'news_css_settings' );
	$raw_cont = isset( $options['news_css-content'] ) ? $options['news_css-content'] : '';
	$content     = wp_kses( $raw_cont, array( '\'', '\"' ) );
	$content     = str_replace( '&gt;', '>', $content );
	echo $content;
	die();
}

add_action( 'plugins_loaded', 'news_css_maybe_print_css' );



?>